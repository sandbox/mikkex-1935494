<?php

/**
 * @file
 * Contains \Drupal\wunder_fine\Plugin\block\block\WunderFineBlock.
 */

namespace Drupal\wunder_fine\Plugin\block\block;


use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a 'Wunder fine' block.
 *
 * @Plugin(
 *   id = "wunder_fine_block",
 *   admin_label = @Translation("Wunder fine block"),
 *   module = "wunder_fine"
 * )
 */
class WunderFineBlock extends BlockBase {
  /**
   * Implements \Drupal\block\BlockBase::build().
   */
  public function build() {
    $form = drupal_get_form('user_login_form');
    
    return array(
      'login_form' => $form,
    );
  }
}
