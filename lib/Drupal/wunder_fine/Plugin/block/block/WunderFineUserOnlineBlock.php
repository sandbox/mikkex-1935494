<?php

/**
 * @file
 * Contains \Drupal\wunder_fine\Plugin\block\block\WunderFineUserOnlineBlock.
 */

namespace Drupal\wunder_fine\Plugin\block\block;

use Drupal\user\Plugin\block\block\UserOnlineBlock;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a "Who's online" block.
 *
 * @todo Move this block to the Statistics module and remove its dependency on
 *   {users}.access.
 *
 * @Plugin(
 *   id = "wunder_fine_user_online_block",
 *   admin_label = @Translation("Wunder fine who's online"),
 *   module = "wunder_fine"
 * )
 */
class WunderFineUserOnlineBlock extends UserOnlineBlock {
  public function blockForm($form, &$form_state) {
    $period = drupal_map_assoc(array(15, 30, 60, 120, 180, 300, 600, 900, 1800, 2700, 3600, 5400, 7200, 10800, 21600, 43200, 86400), 'format_interval');
    $form['user_block_seconds_online'] = array(
      '#type' => 'select',
      '#title' => t('User activity'),
      '#default_value' => $this->configuration['seconds_online'],
      '#options' => $period,
      '#description' => t('A user is considered online for this long after they have last viewed a page.')
    );
    $form['user_block_max_list_count'] = array(
      '#type' => 'select',
      '#title' => t('User list length'),
      '#default_value' => $this->configuration['max_list_count'],
      '#options' => drupal_map_assoc(array(0, 5, 10, 15, 20, 25, 30, 40, 50, 75, 100)),
      '#description' => t('Maximum number of currently online users to display.')
    );
    return $form;
  }
  
}
