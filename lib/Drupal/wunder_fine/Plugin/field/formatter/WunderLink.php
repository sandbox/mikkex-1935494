<?php
/**
 * @file
 * Definition of Drupal\wunder_fine\Plugin\field\formatter\WunderLink.
 */

namespace Drupal\wunder_fine\Plugin\field\formatter;

// These two classes are not recognized by Netbeans as not uses classes because 
// of they are used in the comment part

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

use Drupal\field\Plugin\Type\Formatter\FormatterBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Plugin implementation of the 'email_mailto' formatter.
 *
 * @Plugin(
 *   id = "wunder_link",
 *   module = "wunder_fine",
 *   label = @Translation("Wunder link"),
 *   field_types = {
 *     "email"
 *   }
 * )
 */
class WunderLink extends FormatterBase {
  /**
   * Implements Drupal\field\Plugin\Type\Formatter\FormatterInterface::viewElements().
   */
  public function viewElements(EntityInterface $entity, $langcode, array $items) {
    $elements = array();
    foreach ($items as $delta => $item) {
      if ((!$GLOBALS['user']->uid && !(arg(0) == 'user' && !is_numeric(arg(1))))) {
        $elements[$delta] = array(
          '#type' => 'markup',
          '#markup' => '<span style="color:red">' 
            . t('You need to be logged in to see emailaddresses') . '</span>',
        );
      }
      else {
        $elements[$delta] = array(
        '#type' => 'link',
        '#title' => $item['value'],
        '#href' => 'mailto:' . $item['value'],
      );
     }
    }
    return $elements;
  }
}
